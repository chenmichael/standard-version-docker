FROM node:21-alpine
ARG VERSION
RUN apk add --no-cache \
    git \
    git-lfs
ENV VERSION=${VERSION}
RUN if [ -z "${VERSION}" ]; then echo 'Build arg VERSION must be specified!'; exit 1; fi \
    && npm install --global standard-version@${VERSION}
